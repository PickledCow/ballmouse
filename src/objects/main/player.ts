import Entity from "../../primitives/entity";
import Sprite from "../../primitives/sprite";
import { Vector } from "../../types";

export default class Player extends Entity{
    sprite = new Sprite({
        src: "assets/player/player.svg",
        size: new Vector(51, 56),
        centred: true
    });

    children = [this.sprite];

    position = new Vector(384, 823);

    size = 2.5;
    grazeSize = 50;

    speed = 7;
    focus_speed_multiplier = 0.5;

    tick(delta) {
        this.movement();

        this.position.x = Math.min(Math.max(this.position.x, 12), 768 - 12);
        this.position.y = Math.min(Math.max(this.position.y, 24), 896 - 20);
    }

    private movement() {
        let move = new Vector();

        if (this.game.actionPressed('left'))  move.x -= 1;
        if (this.game.actionPressed('right')) move.x += 1;
        if (this.game.actionPressed('up'))    move.y -= 1;
        if (this.game.actionPressed('down'))  move.y += 1;

        move.normalise();

        let speed_multiplier = 1;

        if (this.game.actionPressed('focus'))
            speed_multiplier = this.focus_speed_multiplier;

        move.multiply(this.speed * speed_multiplier);

        this.position.add(move);
    }


}