import Entity from "../../primitives/entity";
import { Vector, AABB } from "../../types";
import Sprite from "../../primitives/sprite";

export default class Bullet extends Entity {
    isFree: boolean = true;
    visible = false

    direction: Vector = new Vector();
    speed: number = 0;

    // Radius of bullet
    size: number = 0

    // Region of bullet
    region: AABB;

    sprite: Sprite = new Sprite({ src: '/assets/debug/basicbullet.png', centred: true});
    
    children = [this.sprite]

    tick(delta) {
        if (!this.isFree) {
            this.position.x += this.direction.x * this.speed;
            this.position.y += this.direction.y * this.speed;

            if (this.position.x + this.size * 4 < 0 || this.position.x - this.size * 4 > 768 || 
                this.position.y + this.size * 4 < 0 || this.position.y - this.size * 4 > 896) {
                    this.clear();
                }
        }
    }

    clear() {
        this.isFree = true;
        this.visible = false;               
    }
}