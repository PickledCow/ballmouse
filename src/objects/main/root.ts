import Entity from "../../primitives/entity";
import Rect from "../../primitives/rect";
import { Vector, AABB } from "../../types";
import Player from "./player";
import Bullet from "./bullet";
import { test } from "shelljs";

export default class Root extends Entity {
    playfield = new Entity({
        position: new Vector(64, 32)
    })

    maxBullets = 2000

    player = new Player;

    bullets = new Entity;
    freeBullets = [];

    children = [this.playfield]

    constructor(...args) {
        super(args);
        for (let i = 0; i < this.maxBullets; i++) {
            this.bullets.children.push(new Bullet);
        }
        this.playfield.children = [
            this.player,
            this.bullets
        ]
    }

    t = 0
    v = 0
    u = 0

    tick(delta) {  
        this.u++ 
        if (this.u > 30) {
            this.game.score = this.game.fps.toFixed(1);
            this.u -= 30;
        }

        this.getFreeBullets();

        this.t++;
        this.v++;

        
        while (this.t >= 20) {
            const angle = Math.random() * Math.PI * 2;
            this.t -= 20;
            for (let i = 0; i < 60; i++) {
                this.createBullet(384, 150, Math.cos(angle + 2*Math.PI * i / 60), Math.sin(angle + 2*Math.PI * i / 60), 1.2, 6, 0)
            }
        }
        
        super.tick(delta)
    }




    private getFreeBullets() {
        this.freeBullets = [];
        for (let i = 0; i < this.bullets.children.length; i++) {
            if (this.bullets.children[i].isFree) {
                this.freeBullets.push(this.bullets.children[i]);
            }
        }
    }

    private createBullet(px: number, py: number, dx: number, dy: number, speed: number, size: number, layer: number) {
        if (this.freeBullets.length) {
            let b = this.freeBullets.shift();
            b.isFree = false;
            b.visible = true;

            b.position.x = px;
            b.position.y = py;
            b.direction.x = dx;
            b.direction.y = dy;
            b.speed = speed;
            b.size = size;
            b.layer = layer;

            this.bullets.children.splice(this.bullets.children.indexOf(b), 1);
            this.bullets.children.unshift(b);
            
            return b;
        }
    }
}