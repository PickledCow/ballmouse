import Game from "./base";
import Root from "../objects/main/root";

/** Main Game class */
export default class Main extends Game {
    root: Root = new Root({});
    score: number = 0;

    actions = {
        'left': ['KeyJ', 'ArrowLeft'],
        'right': ['KeyL', 'ArrowRight'],
        'up': ['KeyI', 'ArrowUp'],
        'down': ['KeyK', 'ArrowDown'],
        'focus': ['ShiftLeft']
    }

    constructor() {
        super();
        this.el.width  = 1280;
        this.el.height = 960;
        this.el.style.backgroundImage = 'linear-gradient(rgb(211, 228, 255), skyblue, skyblue)';
    }
    render() {
        const ctx = this.ctx;
        super.render();
        ctx.font = "32px sans-serif"
        ctx.fillText(this.score.toString(), 100, 100);
        
        ctx.fillRect(0, 0, 1280, 32);
        ctx.fillRect(0, 928, 1280, 32);
        ctx.fillRect(0, 0, 64, 960);
        ctx.fillRect(832, 0, 448, 960);
    }
    tick(time) {
        super.tick(time);
    }
}