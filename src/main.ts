import Game from './games/main';
import sleep from './sleep';

const el = document.querySelector("#game");
const game = new Game;
el.appendChild(game.el);

let lt = Date.now()

/**
 * The entry render function, used only in this file for requestAnimationFrame
 * @param {number} time Time in ms since first frame rendered. This is automatically inserted by `requestAnimationFrame`
 */
function render() {
    game.render();
    requestAnimationFrame(render);
}

/** Runs the tick function every 50th of a second
 *  Unlike before, we will almost never use `delta` to have deterministic physics
 */

setInterval(() => {
    game.tick(performance.now());
}, 1000 / 60)

requestAnimationFrame(render);

export {render}