export class Vector {
    /**
     *  A 2D Vector class. Doesn't do much at all, pretty much just a helper.
     * @param {number} x 
     * @param {number} y 
     */

    x: number;
    y: number;

    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    // These functions do not affect the original Vector
    getAddition(...values) {
        const self = new Vector(this.x, this.y);
        values.forEach(i => {
            self.x += i.x;
            self.y += i.y;
        });
          return self;
    }

    getSubtraction(...values) {
        const self = new Vector(this.x, this.y);
        values.forEach(i => {
            self.x -= i.x;
            self.y -= i.y;
        });
        return self;
    }

    getMultiplication(value) {
        const self = new Vector(this.x, this.y);
        self.x *= value;
        self.y *= value;
        return self;
    }

    getDivision(value) {
        const self = new Vector(this.x, this.y);
        self.x /= value;
        self.y /= value;
        return self;
    }

    getNormalised() {
        const self = new Vector(this.x, this.y);
        const length = Math.sqrt(self.x ** 2 + self.y ** 2);

        if (length > 0) {
            self.x /= length;
            self.y /= length;
        }

        return self
    }

    // These functions affect this Vector
    add(...values) {
        values.forEach(i => {
            this.x += i.x;
            this.y += i.y;
        });
    }

    subtract(...values) {
        values.forEach(i => {
            this.x -= i.x;
            this.y -= i.y;
        });
    }

    multiply(value) {
        this.x *= value;
        this.y *= value;
        return self;
    }

    divide(value) {
        this.x /= value;
        this.y /= value;
        return self;
    }

    normalise() {
        const length = Math.sqrt(this.x ** 2 + this.y ** 2);

        if (length > 0) {
            this.x /= length;
            this.y /= length;
        }

        return self
    }


    toString() {
        return `Vector2(${this.x},${this.y})`
    }
}

export class AABB {
    /**
     *  A 2D AABB class. Doesn't do much at all, pretty much just a helper.
     * @param {number} x 
     * @param {number} y 
     * @param {number} w
     * @param {number} h
     */

    x: number;
    y: number;
    w: number;
    h: number;

    constructor(x = 0, y = 0, w = 0, h = 0) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    hasPoint(p: Vector) {
        if (p.x > this.x && p.x < this.x + this.w && p.y > this.y && p.y < this.y + this.h) {
            return true;
        } else {
            return false;
        }
    }

    overlaps(o: AABB) {
        if (this.x < o.x + o.w && this.x + this.w > o.x && this.y < o.y + o.h && this.y + this.h > o.y) {
            return true;
        } else {
            return false;
        }
    }

    toString() {
        return `AABB(${this.x},${this.y},${this.w},${this.h})`
    }
}