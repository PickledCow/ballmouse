const { app, BrowserWindow } = require('electron')

function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 880,
    height: 660,
    resizable: false,
    webPreferences: {
      nodeIntegration: true
    },
  })

  // and load the index.html of the app.
  win.loadURL("http://localhost:8080")
  win.removeMenu()
  win.setMenu(null)
}

app.on('ready', createWindow)